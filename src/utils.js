import { Button } from '@mui/material';

// let host_post_debug = "http://127.0.0.1:5000/api/landing/partners";
let host_post_debug = "https://aminimulin.ru/api/landing/partners";

export async function handleSubmit(event) {
    event.preventDefault();
    console.log("Handle form submission");

    let url = host_post_debug;
    console.log(url);

    let res = await fetch(url, 
        {method:"POST", mode: "cors", 
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            name: event.target[0].value,
            email: event.target[2].value,
        }),
    });
    console.log("Get fetch result");

    if (res.status != 200) {
        console.log(JSON.stringify(res))
    } else {
        console.log(await res.json())
    };
}

export const form_submit_button = (
    <Button type="submit" variant="contained" color="primary">
        Отправить
    </Button>
);
