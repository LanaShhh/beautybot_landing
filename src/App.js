import React from 'react';
import { AppBar, Toolbar, Typography, Button, Container, Grid, TextField, Divider, Snackbar } from '@mui/material';
import { handleSubmit, form_submit_button } from './utils';
import SimpleImageSlider from "react-simple-image-slider";


function LandingPage() {
  const [state, setState] = React.useState({open: false});
    
  const { open } = state;

  const handleClick = () => {
      console.log("handling click")
      setState({ open: true });
  };

  const handleClose = () => {
      setState({ open: false });
  };

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{flexGrow: 1}}> BeautyBot </Typography>
          <Button color="inherit"> Сменить язык </Button>
        </Toolbar>
      </AppBar>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}> <Typography variant="h3" gutterBottom textAlign="center"> BeautyBot - личный стилист в твоем кармане! </Typography> </Grid>
          <Grid item xs={12} sx={{display: "flex", justifyContent: "center"}} >
              <Button variant="contained" color="primary" href="https://t.me/tg_beautybot" target="_blank" 
                      sx={{'&:hover': {color: 'white'}}}> 
                  Протестируй бота 
              </Button>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="h4" gutterBottom textAlign="center"> О нас </Typography>
              Не знаешь, подойдет ли помада? 
              BeautyBot подберёт цвет продукта на основе твоей фотографии! 
              Подбери макияж за пару кликов.
          </Grid>
          <Grid item xs={12} md={6} sx={{display: "flex", justifyContent: "center", flex: "1 1 auto"}}>
              <img src="beautybot_example.png" width="50%" ></img>
          </Grid>
          <Grid item xs={12} sx={{display: "flex", justifyContent: "center"}}>
            <Typography variant="h4" gutterBottom textAlign="center"> Бизнесу </Typography>
            <ul sx={{display: "flex", justifyContent: "center"}}>
              <li> Ведрим нашу технологию в Вашу систему </li>
              <li> Обеспечим рост числа клиентов </li>
              <li> Увеличим конверсию онлайн-продаж </li>
              <li> Улучшим качество рекомендации товаров </li>
            </ul>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h4" gutterBottom textAlign="center"> Мы хотим сотрудничать с Вами! </Typography>
            <form onSubmit={(event) => {handleSubmit(event).then(() => {handleClick()});}}>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <TextField required id="name" label="Ваше имя" fullWidth />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField required id="email" label="Email" fullWidth />
                </Grid>
                <Grid item xs={12} align="center">
                  {form_submit_button}
                </Grid>
              </Grid>
            </form>
          </Grid>
          <Grid item container xs={12}>
                <Grid item xs={12}>
                    <Typography variant="h5" gutterBottom align="center"> Contact Us </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1" gutterBottom align="center"> sdzhumlyakova@edu.hse.ru </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1" gutterBottom align="center"> Telegram @lana_shhh </Typography>
                </Grid>
            </Grid>
          </Grid>
      </Container>
      <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          autoHideDuration={2000}
          open={open}
          onClose={handleClose}
          message="Записали, скоро свяжемся с Вами!"
          sx={{'& .MuiSnackbarContent-root': {backgroundColor: '#1976d2',} }}
      />
      <footer style={{ marginTop: '50px', textAlign: 'center' }}>
        <Typography variant="body2">&copy; 2024 BeautyBot. Все права защищены.</Typography>
      </footer>
    </div>
  );
}

export default LandingPage;
